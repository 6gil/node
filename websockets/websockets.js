'use strict';
const timezone = 'Europe/Paris'
let socketIO = require('socket.io')
let moment = require('moment-timezone'), io = null
const database = 'events', host = '127.0.0.1', port = 27017
let mongo = require('mongojs'), collections = ["customers", "professionals", "events"]
global.allClients = [], global.clients = [], global.ObjectId = mongo.ObjectId
global.db = mongo(`${host}:${port}/${database}`, collections), global.CryptoJS = require('crypto-js')
global.hash = (message) => global.CryptoJS.SHA256(message).toString(global.CryptoJS.enc.Base64)
const clientEvents = require('./events/clientEvents'), messageEvents = require('./events/messageEvents')
moment.tz.setDefault(timezone)
module.exports = {
    start: server => {
        io = socketIO.listen(server)
        io.on('connect', socket => {
            /** @namespace socket.request.connection.remoteAddress */
            const ipAddress = String(socket.request.connection.remoteAddress)
            const socketId = String(socket.id)
            let newClient = {
                id: socketId,
                ip: ipAddress,
                date: moment()
            }
            const exists = clients.filter(c => String(c.ip) === ipAddress)
            if (exists.length === 0) {
                clients.push(newClient)
                allClients.forEach(c => c.socket.emit('new-client', newClient))
            }
            newClient.socket = socket
            allClients.push(newClient)
            log(`Client connected id : ${socketId} from ip : ${ipAddress} at ${newClient.date.format('YYYY-MM-DD HH:mm:ss')}`)
            allClients.forEach(c => c.socket.emit('client-number', clients.length))
            socket.on('message-sent', messageEvents.messageSent)
            socket.on('new-name', clientEvents.newName(ipAddress))
            socket.on('disconnect', clientEvents.disconnect(socketId))
            // customer events
            require('./sockets/customerSockets').registerEvents(socket)
            // professional events
            require('./sockets/professionalSockets').registerEvents(socket)
            // login / logout events
            require('./sockets/loginSockets').registerEvents(socket)
            // event events
            require('./sockets/eventSockets').registerEvents(socket)
        })
    }
}
