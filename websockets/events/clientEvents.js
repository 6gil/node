'use strict';
const _ = global.lodash
module.exports = {
    newName: ipAddress => message => {
        global.clients = global.clients.map(c => {
            if (ipAddress === String(c.ip)) {
                c.name = message.name
            }
            return c
        })
        global.allClients = global.allClients.map(c => {
            if (ipAddress === String(c.ip)) {
                c.name = message.name
            }
            return c
        })
    },
    disconnect: socketId => () => {
        const relatedClient = _.chain(global.clients).find(c => socketId === String(c.id)).value()
        global.clients = global.clients.filter(c => socketId !== String(c.id))
        global.allClients = global.allClients.filter(c => socketId !== String(c.id))
        let clientName = String(socketId)
        if (relatedClient !== undefined && relatedClient.name !== undefined) {
            clientName = String(relatedClient.name)
        }
        log(`Client ${clientName} disconnected`)
        allClients.forEach(c => c.socket.emit('message-received',{message:`Client disconnected ${clientName}`}))
    }
}
