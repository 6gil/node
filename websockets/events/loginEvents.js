"use strict";
const _ = global.lodash
module.exports = {
    login: socket => object => {
        log('login', object)
        const login = _.get(object, 'login')
        const now = (new Date).toLocaleDateString('fr-FR')
        const updateObject = { logged: true, updated_at: now, last_login: now }
        let done = 0
        const callback = function (state) {
            if (state < 1) {
                done += 1
            } else {
                done = 0
                socket.emit('failed to login')
            }
        }
        /** @namespace db.customers **/
        db.customers.findAndModify({
            query: {
                $or: [ { pseudo: login }, { email: login } ],
                password: hash(_.get(object, 'password'))
            },
            update: { $set: updateObject },
            new: false,
            multi: false
        }, (err, customer, lastError) => {
            if (!err && customer) {
                return socket.emit(`customer logged`, _.omit(customer, 'password'))
            }
            log('failed to login customer', lastError)
            callback(done)
        })
        /** @namespace db.professionals */
        db.professionals.findAndModify({
            query: {
                siren: login,
                password: hash(_.get(object, 'password'))
            },
            update: { $set: updateObject },
            new: false, multi: false
        }, (err, professional, lastError) => {
            if (!err && professional) {
                return socket.emit(`professional logged`, _.omit(professional, 'password'))
            }
            log(`failed to login professional`, lastError)
            callback(done)
        })
    },
    logout: socket => object => {
        log('logout', object)
        const _id = _.get(object, '_id')
        const queryObject = { _id: ObjectId(_id) }
        const updateObject = { logged: false }
        /** @namespace db.customers **/
        db.customers.findAndModify({
            query: queryObject,
            update: { $set: updateObject },
            new: false,
            multi: false
        }, (err, customer, lastError) => {
            if (!err && customer) {
                return socket.emit(`customer logged out`, _.omit(customer, 'password'))
            }
            log(`customer logout failed : ${JSON.stringify(lastError)}`)
        })
        /** @namespace db.professionals */
        db.professionals.findAndModify({
            query: queryObject,
            update: { $set: updateObject },
            new: false,
            multi: false
        }, (err, professional, lastError) => {
            if (!err && professional) {
                return socket.emit(`professional logged out`, _.omit(professional, 'password'))
            }
            log(`professional logout failed : ${JSON.stringify(lastError)}`)
        })
    }
}
