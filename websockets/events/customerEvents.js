"use strict";
const _ = global.lodash
module.exports = {
    create: socket => object => {
        log('creating customer', object)
        const name = _.get(object, 'name')
        const queryObject = {
            $or: [
                { name: name },
                { email: name }
            ],
            password: hash(object.password)
        }
        const nowDate = (new Date()).toLocaleDateString('fr-FR')
        const createObject = _.chain(object).omit('_id')
        .set('password', hash(_.get(object, 'password')))
        .set('fullname', `${_.get(object, 'firstname')} ${_.get(object, 'lastname')}`)
        .set('created_at', nowDate).set('updated_at', nowDate).value()
        /** @namespace global.db.customers */
        db.customers.findAndModify({
            query: queryObject,
            update: { $setOnInsert: createObject },
            new: true, multi: false, upsert: true
        }, (err,doc,lastError) => {
            if (err) {
                log(`customer create failed ${JSON.stringify(err)}, error: ${JSON.stringify(lastError)}`)
                socket.emit(`customer create failed`,{ error: lastError })
            } else {
                log(`customer created ${JSON.stringify(doc)}`)
                socket.emit(`customer created`, _.omit(doc, 'password'))
            }
        })
    },
    edit: socket => object => {
        log('editing customer', object)
        const queryObject = { _id: ObjectId(_.get(object, '_id')) }
        const updateObject = _.chain(object)
        .omit('_id').set('password',hash(_.get(object, 'password')))
        .set('fullname', `${_.get(object, 'firstname')} ${_.get(object, 'lastname')}`)
        .set('updated_at', (new Date).toLocaleDateString('fr-FR')).value()
        db.customers.findAndModify({
            query: queryObject,
            update: { $set: updateObject },
            new: false, multi: false, upsert: false
        }, (err, customer, lastError) => {
            if (err) {
                log(`customer update failed ${_.clone(object._id)}, error : ${JSON.stringify(lastError)}`)
                socket.emit(`customer update failed`, { _id: _.clone(object._id), error: lastError })
            }
            log(`customer updated ${JSON.stringify(customer)}`)
            socket.emit(`customer updated`, _.omit(customer,'password'))
        })
    },
    delete: socket => id => {
        log(`deleting customer with id ${id}`)
        const queryObject = { _id: ObjectId(id) }
        db.customers.remove(queryObject, true, (err,lastError) => {
            if (err) {
                log(`customer delete failed ${id}, error : ${JSON.stringify(lastError)}`)
                return socket.emit(`customer delete failed`, { _id: _.clone(id), error: lastError })
            }
            log(`customer deleted ${id}`)
            socket.emit(`customer deleted`, { _id: _.clone(id) })
        })
    },
    findByName: socket => name => {
        db.customers.find({
            name: name
        }, (err, customers, lasterror) => {
            if (err) {
                return log(`failed to found customers with name ${name}`)
            }
            socket.emit(`customers searched by name founded`, customers)
        })
    },
    show: socket => id => {
        const queryObject = { _id: ObjectId(id) }
        db.customers.findOne(queryObject, (err, customer, lastError) => {
            if (err) {
                log(`customer not found ${id}, error : ${JSON.stringify(lastError)}`)
                return socket.emit(`customer not found`, { _id: _.clone(id), error: lastError })
            }
            log(`customer found ${JSON.stringify(customer)}`)
            socket.emit(`customer found`, _.omit(customer,'password'))
        })
    },
    list: socket => queryObject => {
        db.customers.find(queryObject, (err, customers, lastError) => {
            if (!err) {
                socket.emit(`list of customers`, customers)
            }
        })
    }
}
