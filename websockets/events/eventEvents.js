"use strict";
const _ = global.lodash
module.exports = {
    create: socket => object => {
        log('creating event', object)
        const queryObject = {
            title: _.get(object, 'title'),
            company: _.get(object, 'company'),

        }
        const nowDate = (new Date).toLocaleDateString('fr-FR')
        const createObject = _.chain(object).omit('_id')
        .set('created_at', nowDate).set('updated_at', nowDate).value()
        /** @namespace global.db.events */
        db.events.findAndModify({
            query: queryObject,
            update: { $setOnInsert: createObject },
            new: true, multi: false, upsert: true
        }, (err, doc, lastError) => {
            if (err) {
                log(`event create failed ${JSON.stringify(err)}, error: ${JSON.stringify(lastError)}`)
                return socket.emit(`event create failed`, { error: lastError })
            }
            log(`event created ${JSON.stringify(doc)}`)
            socket.emit(`event created`, doc)
        })
    },
    edit: socket => object => {
        log(`editing event with id ${_.get(object, '_id')}`, object)
        const queryObject = { _id: ObjectId(_.get(object, '_id')) }
        const updateObject = _.chain(object).omit('_id')
        .set('updated_at', (new Date).toLocaleDateString('fr-FR')).value()
        db.events.findAndModify({
            query: queryObject,
            update: { $set: updateObject },
            new: false, multi: false, upsert: false
        }, (err, event, lastError) => {
            if (err) {
                log(`event update failed ${_.clone(object._id)}, error : ${JSON.stringify(lastError)}`)
                return socket.emit(`event update failed`, { _id: _.clone(object._id), error: lastError })
            }
            log(`event updated ${JSON.stringify(event)}`)
            socket.emit(`event updated`, _.omit(event,'password'))
        })
    },
    delete: socket => id => {
        log(`deleting event with id ${id}`)
        const queryObject = { _id: ObjectId(id) }
        db.events.remove(queryObject, true, (err, lastError) => {
            if (err) {
                log(`event delete failed ${id}, error : ${JSON.stringify(lastError)}`)
                return socket.emit(`event delete failed`, { _id: _.clone(id), error: lastError })
            }
            log(`event deleted ${id}`)
            socket.emit(`event deleted`, { _id: _.clone(id) })
        })
    },
    show: socket => id => {
        log(`showing event with id ${id}`)
        const queryObject = { _id: ObjectId(id) }
        db.events.findOne(queryObject, (err, event, lastError) => {
            if (err) {
                log(`event not found ${id}, error : ${JSON.stringify(lastError)}`)
                return socket.emit(`event not found`, { _id: _.clone(id), error: lastError })
            }
            log(`event found ${JSON.stringify(event)}`)
            socket.emit(`event found`, _.omit(event,'password'))
        })
    },
    searchForNearbyEvents: socket => object => {
        const location = object.location, ID = object._id
        db.customers.update({ _id: ObjectId(ID) }, { $set: {
            location: [ location.lat, location.lng ],
            updated_at: (new Date).toLocaleDateString('fr-FR')
        }})
        if (location.distance === undefined) {
            _.set(location, 'distance', 5000)
        }
        const queryObject = {
            location: {
                $nearSphere: {
                    $geometry: {
                        type : 'Point',
                        coordinates: [ location.lat, location.lng ]
                    },
                    $minDistance: 0,
                    $maxDistance: location.distance
                }
            }
        }
        db.events.find(queryObject, (err, events, lastError) => {
            if (!err) {
                return socket.emit(`events nearby customer found`, events)
            }
            log(`failed to find nearby events`, lastError)
            socket.emit('failed to find nearby events', { error: lastError })
        })
    },
    list: socket => queryObject => {
        db.events.find(queryObject, (err, events, lastError) => {
            if (!err) {
                return socket.emit('list of events', events)
            }
            log(`failed to list events`, lastError)
            socket.emit(`failed to list events`, { error: lastError })
        })
    }
}
