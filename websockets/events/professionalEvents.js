"use strict";
const _ = global.lodash
module.exports = {
    create: socket => object => {
        log('creating professional', object)
        const siren = _.get(object, 'siren')
        const password = _.get(object, 'password')
        const queryObject = {
            siren: siren,
            password: hash(password)
        }
        const nowDate = (new Date).toLocaleDateString('fr-FR')
        const createObject = _.chain(object).omit('_id')
        .set('password', hash(password)).set('created_at', nowDate)
        .set('updated_at', nowDate).value()
        log(`createObject : ${JSON.stringify(createObject)}`)
        /** @namespace db.professionals */
        db.professionals.findAndModify({
            query: queryObject,
            update : { $setOnInsert: createObject },
            new: true, multi: false, upsert: true,
        }, (err, professional, lastError) => {
            if (err) {
                log(`professional create failed ${JSON.stringify(err)}, error: ${JSON.stringify(lastError)}`)
                return socket.emit(`professional create failed`, { error: lastError })
            }
            log(`professional created ${JSON.stringify(professional)}`)
            socket.emit(`professional created`, _.omit(professional, 'password'))
        })
    },
    edit: socket => object => {
        log(`editing professional with id ${_.get(object, '_id')}`)
        const queryObject = { _id: ObjectId(object._id) }
        const updateObject = _.chain(object).omit('_id')
        .set('updated_at', (new Date).toLocaleDateString('fr-FR')).value()
        log(`updateObject : ${JSON.stringify(updateObject)}`)
        db.professionals.findAndModify({
            query: queryObject,
            update: { $set: updateObject },
            new: false, multi: false, upsert: false
        }, (err, professional, lastError) => {
            if (err) {
                log(`professional update failed - _id : ${_.clone(object.id)}, error : ${JSON.stringify(lastError)}`)
                return socket.emit('professional update failed', { _id: _.clone(object.id), error: lastError })
            }
            log(`professional updated - ${JSON.stringify(professional)}`)
            socket.emit('professional updated', _.omit(professional, 'password'))
        })
    },
    delete: socket => id => {
        log(`deleting professional with id ${id}`)
        const queryObject = { _id: ObjectId(id) }
        db.professionals.remove(queryObject, true, (err, lastError) => {
            if (err) {
                log(`professional delete failed - _id : ${_.clone(id)}, error : ${JSON.stringify(lastError)}`)
                return socket.emit('professional delete failed', { _id: _.clone(id), error: lastError })
            }
            log(`professional delete - _id : ${_.clone(id)}`)
            socket.emit('professional delete', { _id: _.clone(id) })
        })
    },
    show: socket => id => {
        log(`showing professional with id ${id}`)
        const queryObject = { _id: ObjectId(id) }
        db.professionals.findOne(queryObject, (err, professional, lastError) => {
            if (err) {
                log(`professional not found - _id : ${_.clone(id)}, error : ${JSON.stringify(lastError)}`)
                return socket.emit('professional not found', { _id: _.clone(id), error: lastError })
            }
            log(`professional found : ${JSON.stringify(professional)}`)
            socket.emit('professional found', _.omit(professional, 'password'))
        })
    },
    list: socket => queryObject => {
        db.professionals.find(queryObject, (err, professionals, lastError) => {
            if (!err) {
                return socket.emit(`list of professionals`, professionals)
            }
            log(`failed to list professional`, lastError)
            socket.emit(`failed to list professional`, { error: lastError })
        })
    }
}
