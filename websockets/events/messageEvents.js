"use strict";
const _ = global.lodash
module.exports = {
    messageSent: element => {
        log(`Text sended by client ${JSON.stringify(element)}`)
        allClients.forEach(client => client.socket.emit('message-received', element))
    }
}
