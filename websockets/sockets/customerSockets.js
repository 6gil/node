"use strict";
const customerEvents = require('./../events/customerEvents')
module.exports = {
    registerEvents: socket => {
        socket.on(`find customer by name`, customerEvents.findByName(socket))
        socket.on('create customer', customerEvents.create(socket))
        socket.on('edit customer', customerEvents.edit(socket))
        socket.on('show customer', customerEvents.show(socket))
        socket.on('delete customer', customerEvents.delete(socket))
        socket.on(`list customers`, customerEvents.list(socket))
    }
}
