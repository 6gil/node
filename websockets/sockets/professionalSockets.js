"use strict";
const professionalEvents = require('./../events/professionalEvents')
module.exports = {
    registerEvents: socket => {
        socket.on('create professional', professionalEvents.create(socket))
        socket.on('edit professional', professionalEvents.edit(socket))
        socket.on('show professional', professionalEvents.show(socket))
        socket.on('delete professional', professionalEvents.delete(socket))
        socket.on('list professional', professionalEvents.list(socket))
    }
}