"use strict";
const eventEvents = require('./../events/eventEvents')
module.exports = {
    registerEvents: socket => {
        socket.on('search for nearby events', eventEvents.searchForNearbyEvents(socket))
        socket.on(`list events`, eventEvents.list(socket))
        socket.on('create event', eventEvents.create(socket))
        socket.on('edit event', eventEvents.edit(socket))
        socket.on('show event', eventEvents.show(socket))
        socket.on('delete event', eventEvents.delete(socket))
    }
}