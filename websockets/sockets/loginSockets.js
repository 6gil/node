"use strict";
const loginEvents = require('./../events/loginEvents')
module.exports = {
    registerEvents: socket => {
        socket.on('login', loginEvents.login(socket))
        socket.on('logout', loginEvents.logout(socket))
    }
}