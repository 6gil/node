"use strict";
const gulp = require('gulp')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const uglifyCss = require('gulp-uglifycss')
const browserify = require('gulp-browserify')
const watch = require('gulp-watch')
gulp.task('babel', () => {
    return gulp.src('./assets/js/script.js')
    .pipe(babel())
    .pipe(browserify({
        insertGlobals: true,
        debug: true
    }))
    .pipe(rename('compiled.js'))
    .pipe(gulp.dest('./assets/js'))
})

gulp.task('js', ['babel'], () => {
    return gulp.src([
        /*'./bower_components/socket.io-client/dist/socket.io.min.js',*/
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/boostrap/dist/js/boostrap.min.js',
        './assets/js/compiled.js'
    ])
    .pipe(concat('concat.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('compiled.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'))
});

gulp.task('css', () => {
    return gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        './bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
        './assets/css/style.css'
    ])
    .pipe(concat('concat.css'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('compiled.min.css'))
    .pipe(uglifyCss())
    .pipe(gulp.dest('./dist'));
})

gulp.task('watch', () => {
    return gulp.watch('./assets/js/script.js', ['js'])
})

gulp.task('default', ['js','css','watch'], () => {});
