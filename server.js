'use strict';
const PORT = 1337
let express = require('express')
let app = express()
let server = require('http').createServer(app)
let routes = require('./routes/routes')
const lodash = global.lodash = require('lodash')
global.log = function () {
	lodash.each(arguments, argument => {
		console.log(argument)
	})
}
// api rest
app.use('/', routes)
// websockets
let websockets = require('./websockets/websockets').start(server)
server.listen(PORT)
log(`Server listening on port ${PORT}`)
