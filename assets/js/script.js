"use strict";
window._ = require('lodash')
import * as io from 'socket.io-client'
//import * as CryptoJS from 'crypto-js'
import * as _ from 'lodash'
const env = 'prod' // 'prod'
const serverHost = (env === 'dev') ? '127.0.0.1' : '193.70.1.205'
const serverPort = 1337
//const hash = (message) => CryptoJS.SHA256(message).toString(CryptoJS.enc.Base64)
const socket = io.connect(`http://${serverHost}:${serverPort}`)
window.socket = socket
const log = function () { _.each(arguments, argument => console.log(argument)) }
let customerObject = {}
socket.on('connect', () => socket.emit('join', 'Hello World from client'))
socket.on('new-client', client => log(`Nouveau client dans la salle ${JSON.stringify(client)}`))
socket.on('alert', message => log(message))
socket.on('client-number', nb => log(`nombre de clients connectés : ${nb}`))
socket.on('message-received', element => log(`Message received by server "${element.message}"`))
//socket.on('customer created', id => alert(`customer created ${id}`))
socket.on('customer created', customer => log(`customer created: ${JSON.stringify(customer)}`))
socket.on('customer create failed', object => log(`customer create failed : ${JSON.stringify(object)}`))
socket.on('failed to login', () => log('failed to login'))
socket.on('customer logged', customer => {
    log(`customer logged : ${JSON.stringify(customer)}`)
    customerObject = customer
})
socket.on('professional logged', professional => log(`professional logged : ${JSON.stringify(professional)}`))
socket.on('customer logged out', customer => log(`customer with id ${_.get(customer, '_id')} has been logged out`))
socket.on('professional logged out', professional => log(`professional with id ${_.get(professional,'_id')} has been logged out`))
socket.on('customer found', customer => {
    log(`customer found : ${JSON.stringify(customer)}`)
    window.c = customer
})
$('#show_customer').click( e => {
    e.preventDefault()
    const _id = valueOfField('id')
    socket.emit('show customer', _id)
})
$('#login').click( e => {
    e.preventDefault()
    const customer = {
        login: 'jubasse',
        password: '1234'
    }
    socket.emit('login', customer)
})
$('#logout').click( e => {
    e.preventDefault()
    if (!customerObject || customerObject._id === undefined) {
        alert('You must login before logout')
        return false
    }
    const object = {
        _id: _.get(customerObject,'_id')
    }
    socket.emit('logout', object)
})
$('form').submit( e => {
    e.preventDefault()
    const message = $('#chat_input').val()
    if (message !== undefined && message.length > 0) {
        socket.emit('message-sent', { message: message })
    }
    const name = $('#name').val()
    if (name !== undefined && name.length > 0) {
        socket.emit('new-name', { name: name })
    }
})
const valueOfField = field => $(`#customer_${field}`).val().toString()
$('#customer_form').submit( e => {
    e.preventDefault()
    const lastname = valueOfField('lastname')
    const firstname = valueOfField('firstname')
    const password = valueOfField('password')
    const email = valueOfField('email')
    const login = valueOfField('login')
    if (!lastname || !firstname || !password || !email || !login) {
        alert('You must fill all the fields')
        return false
    }
    const customer = {
        login: login,
        email: email,
        password: password,
        lastname: lastname,
        firstname: firstname,
        fullname: `${firstname} ${lastname}`
    }
    socket.emit('create customer', customer)
})