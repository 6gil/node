'use strict';
let path = require('path')
module.exports = (() => {
    let express = require('express')
    let router = express.Router()
    router.use('/static', express.static(path.resolve( __dirname + '/../dist/' )))
    router.get('/', (req, res, next) => {
        res.sendFile(path.resolve( __dirname + '/../templates/index.html' ))
    })
    router.get('/user', (req, res, next) => {
      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify({message: 'test'}))
    })
    return router
})()
